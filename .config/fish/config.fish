function fish_greeting
    fortune -a | cowsay
    neofetch
end

funcsave fish_greeting

alias config="/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME"
alias matlab="/home/m0rtal/.local/bin/MATLAB/R2021b/bin/matlab"

fish_vi_key_bindings

#export IDF_PATH=/home/m0rtal/esp/esp-idf/
#source /home/m0rtal/esp/esp-idf/export.fish
