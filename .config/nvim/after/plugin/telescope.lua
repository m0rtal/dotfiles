local builtin = require('telescope.builtin')

-- Open up the default telescope screen
vim.keymap.set('n', '<leader>ff', builtin.find_files, {})

-- Grep search in files before showing telescope.
vim.keymap.set('n', '<leader>fs', function()
	builtin.grep_string({search = vim.fn.input("grep > ") });
end)

-- This keybinding is useful to only fuzzy find in git files, TODO: add back in later
-- vim.keymap.set('n', '<C-p>', builtin.git_files, {})
