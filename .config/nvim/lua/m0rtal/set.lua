-- This file contains all sets (options) that I prefer in vim/nvim

-- Set leader key to SPACE
vim.g.mapleader = " "

-- Enables relative line numbers
vim.opt.nu = true
vim.opt.relativenumber = true

-- Tab settings
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

-- Search options
vim.opt.incsearch = true    -- Incremental search highlighting
--vim.opt.hlsearch = false  -- Disable hl after search entered

-- Don't wrap by default
vim.opt.wrap = false

-- Set term colors (also done elsewhere, but here for safety...)
vim.opt.termguicolors = true

-- Makes sure at least 8 lines above and below cursor are visible always
vim.opt.scrolloff = 8

