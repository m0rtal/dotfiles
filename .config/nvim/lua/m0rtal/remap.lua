-- This file contains my remaps and custom keybindings

-- Already set in set.lua with the other options
-- vim.g.mapleader = " "

-- <leader-pv opens Netrw file manager
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)

-- Horizontal and vertical splits
vim.keymap.set("n", "<leader>ws", vim.cmd.sp)
vim.keymap.set("n", "<leader>wv", vim.cmd.vsp)

-- Moving around splits
-- NOTE: the call to wincmd must be wrapped in a function() to work
vim.keymap.set("n", "<leader>wh", function() vim.cmd.wincmd('h') end)
vim.keymap.set("n", "<leader>wj", function() vim.cmd.wincmd('j') end)
vim.keymap.set("n", "<leader>wk", function() vim.cmd.wincmd('k') end)
vim.keymap.set("n", "<leader>wl", function() vim.cmd.wincmd('l') end)

-- Rotating splits
vim.keymap.set("n", "<leader>wr", function() vim.cmd.wincmd('r') end)

-- Alternative Vim Commentary bindings
vim.keymap.set("n", "<leader>cl", "<Plug>CommentaryLine") -- gcc
vim.keymap.set("v", "<leader>cl", "<Plug>Commentary")     -- gc

-- Clear search highlight
vim.keymap.set("n", "<leader>sc", vim.cmd.noh) 
